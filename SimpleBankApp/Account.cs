﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SimpleBankApp
{
	//TODO: Define an Account class as described by the UML diagram provided with the 
	//ATM Simulator Case Study
	class Account
	{
		private string _accountHolderName;
		private int _acctNo;
		private double _annualIntrRate;
		private double _balance;
		public Account(int acctNo, string acctHolderName)
		{
			_accountHolderName = acctHolderName;
			_acctNo = acctNo;
		}
		public string AccountHolderName
		{
			get { return _accountHolderName; }
		}
		public int AccountNumber
		{
			get { return _acctNo; }
		}
		public virtual double AnnualInterestRate
		{
			get { return _annualIntrRate; }
			set { _annualIntrRate = value; }
		} 
		public double Balance
		{
			get { return _balance; }
		}
		public double Deposit(double amount)
		{
			return Balance + amount;
		}
		public virtual double Withdrawl(double amount)
		{
			return Balance - amount;
		}
	}
}
