﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

namespace SimpleBankApp
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class MainPage : Page
    {
        //TODO: define a field variable of type Bank
        private Bank _bank = new Bank();
        public MainPage()
        {
            this.InitializeComponent();
        }

        private void OnClick(object sender, RoutedEventArgs e)
        {
            Account accountSetup = _bank.OpenAccount(_txtAcctHolder.Text, (string)_cmbAcctType.SelectedItem, int.Parse(_txtAcctNo.Text));
            _lstAccountList.Items.Add(accountSetup.AccountNumber);
        }

        private void OnSelection(object sender, RoutedEventArgs e)
        {
            
        }

        //TODO: define an event handler invoked when the Create Account button
        //is clicked. Implement the event handler that obtains the account data and
        //create an account object. Add the account to the bank and display the account
        //in the list of accounts (_lstAccountList) by adding it to its Items collection
    }
}
