﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SimpleBankApp
{	
	//TODO: Define the Bank class that manages a list of accounts and provides
	//access to those accounts
	class Bank
	{
		private Dictionary<int, Account> _accountList;
		public Bank()
		{
			_accountList = new Dictionary<int, Account>();
		}
		public Account OpenAccount(string accountName, string accountType, int accountNo)
		{
			Account newAccount = new Account(accountNo, accountName);
			if (accountType == "Checking")
			{
				newAccount = new ChequingAccount(accountNo,accountName);
			}
			else if (accountType == "Savings")
			{
				newAccount = new SavingsAccount(accountNo, accountName);
			}
			_accountList.Add(accountNo, newAccount);
			return newAccount;
		}
	}
}
