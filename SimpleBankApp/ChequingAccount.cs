﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SimpleBankApp
{
	class ChequingAccount:Account
	{
		//TODO: Define an ChequingAccount class as described by the UML diagram provided with the 
		//ATM Simulator Case Study. Derive the ChequingAccount class from Account
		public ChequingAccount(int accountNo,string acctHolderName) : base(accountNo, acctHolderName)
		{
		}

		public override double AnnualInterestRate 
		{
			set
			{
				base.AnnualInterestRate = value;
			}
		}
		public override double Withdrawl(double amount)
		{
			return base.Withdrawl(amount);
		}
	}
}
