﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SimpleBankApp
{
	//TODO: Define an SavingsAccount class as described by the UML diagram provided with the 
	//ATM Simulator Case Study. Derive the SavingsAccount class from Account
	class SavingsAccount : Account
	{
		public SavingsAccount(int accountNo, string acctHolderName) : base(accountNo, acctHolderName)
		{
		}
		public override double AnnualInterestRate
		{
			set
			{
				base.AnnualInterestRate = value;
			}
		}
		public override double Withdrawl(double amount)
		{
			return base.Withdrawl(amount);
		}
	}
}
